# CSDA tutorial - Probability distributions

## Learning objectives:  
 
- know what sampling and statistical distributions are,
- be aware of a few common distributions of biological data,
- be able to use visualize the distribution of your data in R and
- be able to decide on a suitable distribution to model your data with.

## How to run the tutorial

**Manually:**  

- download / clone this repository
- Open RStudio  
- Install the `learnr` package  
- open the file `Tutoriao-day2.Rmd` in Rstudio  
- Click *Run Document*  

**Online:**  

- Connect with VPN, if you're not connected to the EMBL ethernet
- Run the tutorial [here](https://cbbcs-01.embl.de/shinyapps/app/03_csda_tutorial_2)
