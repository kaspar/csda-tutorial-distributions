FROM rocker/verse:4.1.2

RUN R -e "install.packages(c('vcd', 'fitdistrplus', 'learnr'))"

ADD tutorial /home/rstudio

# App on port 6880
EXPOSE 6880

# Create unprivileged user
RUN useradd -m learnr -d /home/learnr

# Allow unprivileged user access to the app directory
RUN chown -R learnr /home/rstudio

# Run as user learnr to avoid running as root
USER learnr

CMD ["R", "-e", "rmarkdown::run('/home/rstudio/Tutorial-day2.Rmd', shiny_args=list(host = '0.0.0.0', port = 6880))"]
